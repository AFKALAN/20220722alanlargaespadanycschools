package com.example.a20220722_alanlargaespada_nycschools.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220722_alanlargaespada_nycschools.databinding.ViewHolderSchoolBinding
import com.example.a20220722_alanlargaespada_nycschools.model.School

class SchoolAdapter(val schoolsList: List<School>,val onClick: (Int) -> Unit): RecyclerView.Adapter<SchoolViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewHolderSchoolBinding.inflate(layoutInflater, parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.setData(schoolsList[position])
        holder.binding.btnDetails.setOnClickListener {
            onClick(position)
        }
    }

    override fun getItemCount() = schoolsList.size
}