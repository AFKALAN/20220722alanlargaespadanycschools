package com.example.a20220722_alanlargaespada_nycschools.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220722_alanlargaespada_nycschools.R
import com.example.a20220722_alanlargaespada_nycschools.databinding.ActivityMainBinding
import com.example.a20220722_alanlargaespada_nycschools.model.School
import com.example.a20220722_alanlargaespada_nycschools.viewModel.SchoolViewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    lateinit var schoolViewModel: SchoolViewModel
    lateinit var adapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.rvSchools.layoutManager = LinearLayoutManager(this)
        schoolViewModel = ViewModelProvider(this)[SchoolViewModel::class.java]
        setContentView(binding.root)

        schoolViewModel.getSchools()

        setupObserver()
    }

    private fun setupObserver() {
        schoolViewModel.schoolsLiveData.observe(this){
            adapter = SchoolAdapter(it){position -> onClick(position, it)}
            binding.rvSchools.adapter = adapter
        }
    }

    private fun onClick(position: Int, schoolsList: List<School>) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra("school", schoolsList[position])
        startActivity(intent)
    }
}