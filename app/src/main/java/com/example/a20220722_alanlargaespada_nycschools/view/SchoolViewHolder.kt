package com.example.a20220722_alanlargaespada_nycschools.view

import androidx.recyclerview.widget.RecyclerView
import com.example.a20220722_alanlargaespada_nycschools.databinding.ViewHolderSchoolBinding
import com.example.a20220722_alanlargaespada_nycschools.model.School

class SchoolViewHolder(val binding: ViewHolderSchoolBinding): RecyclerView.ViewHolder(binding.root){
    fun setData(school: School){
        binding.tvName.text = school.school_name
        binding.tvLocation.text = school.location
    }
}