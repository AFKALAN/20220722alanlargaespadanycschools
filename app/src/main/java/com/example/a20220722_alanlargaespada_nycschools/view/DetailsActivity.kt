package com.example.a20220722_alanlargaespada_nycschools.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.a20220722_alanlargaespada_nycschools.databinding.ActivityDetailsBinding
import com.example.a20220722_alanlargaespada_nycschools.model.School
import com.example.a20220722_alanlargaespada_nycschools.viewModel.DetailsViewModel

class DetailsActivity : AppCompatActivity() {
    lateinit var binding: ActivityDetailsBinding
    lateinit var detailsViewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        detailsViewModel = ViewModelProvider(this)[DetailsViewModel::class.java]
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val school = intent.getParcelableExtra<School>("school")
        detailsViewModel.getDetails(school?.dbn.toString())

        binding.tvName.text = school?.school_name
        setupObservers()

    }

    @SuppressLint("SetTextI18n")
    private fun setupObservers() {
        detailsViewModel.scoresLiveData.observe(this){
            binding.apply {
                tvScores.text = "Math Average: ${it.sat_math_avg_score}" +
                        "\n Reading Average: ${it.sat_critical_reading_avg_score}" +
                        "\n Writing Average: ${it.sat_writing_avg_score}"
            }
        }

        detailsViewModel.noScore.observe(this){
            binding.tvScores.text = it
        }
    }
}