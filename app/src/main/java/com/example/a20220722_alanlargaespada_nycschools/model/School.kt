package com.example.a20220722_alanlargaespada_nycschools.model

import android.os.Parcel
import android.os.Parcelable

data class School(
    val dbn: String,
    val extracurricular_activities: String,
    val graduation_rate: String,
    val location: String,
    val overview_paragraph: String,
    val phone_number: String,
    val school_email: String,
    val school_name: String,
    val website: String
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: "",
        parcel.readString()?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(extracurricular_activities)
        parcel.writeString(graduation_rate)
        parcel.writeString(location)
        parcel.writeString(overview_paragraph)
        parcel.writeString(phone_number)
        parcel.writeString(school_email)
        parcel.writeString(school_name)
        parcel.writeString(website)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}