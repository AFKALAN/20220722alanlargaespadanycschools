package com.example.a20220722_alanlargaespada_nycschools.model

import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitServices {
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(): List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getDetails(@Query("dbn") dbn: String): DetailsResponse
}