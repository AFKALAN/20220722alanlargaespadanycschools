package com.example.a20220722_alanlargaespada_nycschools.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220722_alanlargaespada_nycschools.model.RetrofitClient
import com.example.a20220722_alanlargaespada_nycschools.model.School
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class SchoolViewModel: ViewModel() {
    val schoolsLiveData = MutableLiveData<List<School>>()

    fun getSchools(){
        viewModelScope.launch(IO) {
            schoolsLiveData.postValue(RetrofitClient.retrofitServices.getSchools())
        }
    }
}