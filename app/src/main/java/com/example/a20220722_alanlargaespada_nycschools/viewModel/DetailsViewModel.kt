package com.example.a20220722_alanlargaespada_nycschools.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220722_alanlargaespada_nycschools.model.DetailsResponseItem
import com.example.a20220722_alanlargaespada_nycschools.model.RetrofitClient
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class DetailsViewModel: ViewModel() {
    val scoresLiveData = MutableLiveData<DetailsResponseItem>()
    val noScore = MutableLiveData<String>()

    fun getDetails(dbn: String){
        viewModelScope.launch(IO){
            val details = RetrofitClient.retrofitServices.getDetails(dbn)
            if (details.size > 0){
                scoresLiveData.postValue(RetrofitClient.retrofitServices.getDetails(dbn)[0])
            }else{
                noScore.postValue("No scores found for this school")
            }
        }
    }
}